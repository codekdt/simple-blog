<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/posts/create', 'PostsController@create')->name('posts.create');
    Route::post('/posts', 'PostsController@store')->name('posts.store');
    Route::get('/posts/manager', 'PostsController@manager')->name('posts.manager');
    Route::get('/posts/{id}/edit', 'PostsController@edit')->name('posts.edit');
    Route::put('/posts/{id}', 'PostsController@update')->name('posts.update');
    Route::delete('/posts/{id}', 'PostsController@destroy')->name('posts.destroy');

    Route::middleware(['is_admin'])->group(function () {
        Route::get('/review', 'PostsController@review')->name('posts.review');
        Route::put('posts/{id}/publish-at', 'PostsController@publishAt')->name('posts.publishAt');
        Route::put('posts/{id}/publish', 'PostsController@publish')->name('posts.publish');
        Route::put('posts/{id}/unpublish', 'PostsController@unpublish')->name('posts.unpublish');
    });
});

Route::get('/posts/{slug}', 'PostsController@show')->name('posts.show');