Simple Blog use [Laravel 5.6](http://laravel.com), created by [Hai Dang].

## Finished feature
* Everyone could see list of posts and check their detail
* People could register for new account and login/logout to the system
* Registered users could CRUD their posts.
* Posts' body understand markdown syntax and could render properly
* Admin could see a list of created posts
* Admin could publish or unpublish created posts
* Only published posts would be display in public listing page
* Admin could see highlighting unpublished posts in list of all posts
* Admin could filter/order posts by date or status
* Admin could schedule post publishing. E.g I want publish this post automatically in tomorrow 9AM
* Unit test
