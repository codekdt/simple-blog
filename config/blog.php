<?php

return [
    'posts' => [
        'limit' => 5,
    ],

    'filters' => [
        'statuses' => [
            'publish' => 'Published',
            'unpublish' => 'Unpublished',
        ],

        'sorts' => [
            'newest' => 'Newest',
            'oldest' => 'Oldest',
        ],
    ]
];