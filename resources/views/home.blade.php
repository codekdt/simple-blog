@extends('layouts.app')

@section('title', 'Blog home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="my-4">Blog Home
            </h1>

            @foreach ($posts as $post)
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">{{ $post->title }}</h5>
                    <p class="card-text">@markdown($post->content)</p>
                    <a href="{{ $post->detail_url }}" class="btn btn-primary">Read more ...</a>
                </div>
                <div class="card-footer text-muted">
                    Posted on {{ $post->published_at->format('F d, Y') }} by <span class="text-primary">{{ $post->user->name }}</span>
                </div>
            </div>
            @endforeach

            {{ $posts->links() }}
        </div>
    </div>
</div>
@endsection
