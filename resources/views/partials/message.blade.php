@if (session()->has('flash_notification'))
    <script type="text/javascript">
        swal({
            title: "{{ session('flash_notification')->first()->title }}",
            text: "{!! session('flash_notification')->first()->message !!}",
            type: "{{ session('flash_notification')->first()->level }}",
            timer: 1700,
            buttons: false
        });
    </script>
    {{ session()->forget('flash_notification') }}
@endif