@extends('layouts.app')

@section('title', 'Create post')

@push('styles')
    <link href="{{ asset('plugins/simplemde/simplemde.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('plugins/simplemde/simplemde.min.js') }}"></script>

    <script>
        var simplemde = new SimpleMDE();
    </script>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Create blog</h2>
                <form id="contact-form" method="post" action="{{ route('posts.store') }}" role="form" novalidate>
                    @csrf

                    @include('partials.errors')

                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Title *</label>
                                    <input id="title" type="text" name="title" class="form-control" placeholder="Please enter the title *" required="required" data-error="Title is required." value="{{ old('title') }}">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="content">Content *</label>
                                <textarea id="content" name="content" class="form-control" placeholder="" rows="4" required="required" data-error="Content" value="{{ old('content') }}"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-primary btn-send" value="Save blog">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted"><strong>*</strong> These fields are required.</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection