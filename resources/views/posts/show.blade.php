@extends('layouts.app')

@section('title', $post->title)

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <!-- Title -->
                <h1 class="mt-4">{{ $post->title }}</h1>

                <!-- Author -->
                <p class="lead">
                    by
                    <a href="#">{{ $post->user->name }}</a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p>Posted on {{ $post->published_at->format('F d, Y') }} at {{ $post->published_at->format('h:i A') }}</p>

                <hr>

                <!-- Preview Image -->
                {{--<img class="img-fluid rounded" src="http://placehold.it/900x300" alt="">--}}

                {{--<hr>--}}

                <!-- Post Content -->
                <p class="lead">
                    @markdown($post->content)
                </p>

                </div>

            </div>
        </div>
    </div>
@endsection