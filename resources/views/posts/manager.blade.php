@extends('layouts.app')

@section('title', 'Manager')

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.btn-delete').on('click', function (e) {
                e.preventDefault();

                var url = $(this).data('url');

                swal({
                    title: "Are you sure?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: true
                }).then(function () {
                    $.ajax({
                        url : url,
                        type : 'DELETE',
                        beforeSend: function (xhr) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                        }
                    }).always(function (data) {
                        window.location.reload();
                    });
                });
            });
        });
    </script>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="my-4">Manager my blog
                </h1>

                @foreach ($posts as $post)
                    <div class="card mb-4 @if (! $post->isPublished()) bg-light @endif">
                        <div class="card-header">
                            <h5>{{ $post->title }}

                            @if ($post->isPublished())
                            <span class="badge badge-primary float-right">Published</span></h5>
                            @else
                                <span class="badge badge-secondary float-right">Unpublished</span></h5>
                            @endif

                        </div>
                        <div class="card-body ">
                            {{--<h5 class="card-title"></h5>--}}
                            <p class="card-text">@markdown($post->content)</p>
                            {{--<a href="{{ $post->detail_url }}" class="btn btn-primary">Read more ...</a>--}}
                        </div>
                        <div class="card-footer text-muted">
                            @if ($post->isPublished())
                                Published at {{ $post->published_at->format('F d, Y h:i A') }}</span>
                            @else
                                Unpublished
                            @endif
                            <div class="float-right">
                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info btn-sm">Edit</a>
                            <button class="btn btn-danger btn-sm btn-delete" data-url="{{ route('posts.destroy', $post->id) }}">Delete</button>
                            </div>
                        </div>
                    </div>
                @endforeach

                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection
