@extends('layouts.app')

@section('title', 'Admin')

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush

@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>

        $(document).ready(function() {
            function cb(start, end) {
                $('input[name="datetime"] span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('input[name="datetime"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('input[name="datetime"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="datetime"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $('.btn-publish-at').on('click', function (e) {
                var url = $(this).data('url');

                swal({
                    title: "Pick datetime to publish this post..",
                    showCancelButton: true,
                    confirmButtonText: 'Update',
                    html:'<input id="datetimepicker" type="text" class="form-control" autofocus value="{{ now()->addDay()->setTime(9, 0)->format('m/d/Y h:i A') }}">',
                    onOpen: function() {
                        $('#datetimepicker').daterangepicker({
                            singleDatePicker: true,
                            timePicker: true,
                            showDropdowns: true,
                            locale: {
                                format: 'MM/DD/YYYY hh:mm A'
                            }
                        });
                    },
                    preConfirm: (datetime) => {

                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url : url,
                            type : 'PUT',
                            data: {'datetime': $('#datetimepicker').val()},
                            beforeSend: function (xhr) {
                                var token = $('meta[name="csrf-token"]').attr('content');
                                if (token) {
                                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            }
                        }).done(function (data) {
                            window.location.reload();
                        }).fail(function () {
                            swal('', 'Error', 'error');
                        });
                    }
                });
            });

            $('.btn-publish').on('click', function (e) {
                e.preventDefault();

                var url = $(this).data('url');

                $.ajax({
                    url : url,
                    type : 'PUT',
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf-token"]').attr('content');
                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    }
                }).always(function (data) {
                    window.location.reload();
                });
            });

            $('.btn-unpublish').on('click', function (e) {
                e.preventDefault();

                var url = $(this).data('url');

                swal({
                    title: "Are you sure?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: true
                }).then((ok) => {
                    if (ok.value) {
                        $.ajax({
                            url : url,
                            type : 'PUT',
                            beforeSend: function (xhr) {
                                var token = $('meta[name="csrf-token"]').attr('content');
                                if (token) {
                                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                                }
                            }
                        }).always(function (data) {
                            window.location.reload();
                        });
                    }
                });
            });
        });
    </script>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="my-4">All blog
                </h1>

                <form id="contact-form" method="get" role="form" novalidate>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="datetime" placeholder="Date" autocomplete="off" value="{{ request('datetime') }}">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control" name="status">
                                        <option value=''>All status</option>
                                        @foreach (config('blog.filters.statuses') as $status => $name)
                                            @if (request('status') == $status)
                                                <option value="{{ $status }}" selected> {{ $name }} </option>
                                            @else
                                                <option value="{{ $status }}"> {{ $name }} </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <select class="form-control" name="sort">
                                        <option value="">Sort by..</option>
                                        @foreach (config('blog.filters.sorts') as $key => $name)
                                            @if (request('sort') == $key)
                                                <option value="{{ $key }}" selected> {{ $name }} </option>
                                            @else
                                                <option value="{{ $key }}"> {{ $name }} </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 float-right">
                                <input type="submit" class="btn btn-primary btn-send" value="Filter post">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>

                @foreach ($posts as $post)
                    <div class="card mb-4 @if (! $post->isPublished()) border-warning bg-light @endif">
                        <div class="card-header">
                            <h5>{{ $post->title }}

                                @if ($post->isPublished())
                                    <span class="badge badge-primary float-right">Published</span></h5>
                            @else
                                <span class="badge badge-secondary float-right">Unpublished</span></h5>
                            @endif

                        </div>
                        <div class="card-body ">
                            {{--<h5 class="card-title"></h5>--}}
                            <p class="card-text">@markdown($post->content)</p>
                            {{--<a href="{{ $post->detail_url }}" class="btn btn-primary">Read more ...</a>--}}
                        </div>
                        <div class="card-footer text-muted">
                            @if ($post->isPublished())
                                Published at {{ $post->published_at->format('F d, Y h:i A') }} by <span class="text-primary"> {{ $post->user->name }} </span></span>
                            @else
                                Unpublished
                            @endif
                            <div class="float-right">
                                @if ($post->isPublished())
                                <button class="btn btn-dark btn-sm btn-unpublish" data-url="{{ route('posts.unpublish', $post->id) }}">Unpublish this post</button>
                                @else
                                <button class="btn btn-primary btn-sm btn-publish" data-url="{{ route('posts.publish', $post->id) }}">Publish now</button>

                                <button class="btn btn-success btn-sm btn-publish-at" data-url="{{ route('posts.publishAt', $post->id) }}">Publish at...</button>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach

                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection
