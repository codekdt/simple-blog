<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')
            ->published()
            ->latest()
            ->simplePaginate(config('posts.limit', 5));

        return view('home', compact('posts'));
    }
}
