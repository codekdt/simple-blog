<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function show($slug)
    {
        $post = Post::with('user')->findBySlugOrFail($slug);

        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $user = auth()->user();

        $user->posts()->create($request->all());

        flash('Created blog successfully.')->success();

        return redirect()->route('posts.manager');
    }

    public function manager(Request $request)
    {
        $user= auth()->user();

        $posts = $user->posts()->latest('created_at')->paginate(5);

        return view('posts.manager', compact('posts'));
    }

    public function edit($id)
    {
        $post = auth()->user()->posts()->findOrFail($id);

        return view('posts.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $post = auth()->user()->posts()->findOrFail($id);

        $post->update($request->all());

        flash('Blog updated successfully.')->success();

        return redirect()->route('posts.edit', $id);
    }

    public function destroy($id)
    {
        $post = auth()->user()->posts()->findOrFail($id);

        $post->delete();

        flash('Blog deleted successfully.')->success();

        return response()->json([]);
    }

    public function review(Request $request)
    {
        $posts = Post::with('user')
            ->filter($request)
            ->paginate(5);

        return view('posts.review', compact('posts'));
    }

    public function publishAt($id, Request $request)
    {
        $this->validate($request, [
            'datetime' => 'required|date_format:"m/d/Y h:i A"',
        ]);

        $post = Post::findOrFail($id);

        $post->publish($request->datetime);

        flash('Blog was set schedule publish time.')->success();

        return response()->json([]);
    }

    public function publish($id)
    {
        $post = Post::findOrFail($id);

        $post->publish();

        flash('Blog was published successfully.')->success();

        return response()->json([]);
    }

    public function unpublish($id)
    {
        $post = Post::findOrFail($id);

        $post->unpublish();

        flash('Blog was unpublished.')->success();

        return response()->json([]);
    }
}