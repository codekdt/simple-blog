<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use Sluggable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'user_id', 'content', 'published_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'published_at'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ]
        ];
    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include published posts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $includeWillPublish
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query, $includeWillPublish = false)
    {
        return $query->whereNotNull('published_at')
            ->where(function ($q) use ($includeWillPublish) {
                if (! $includeWillPublish) {
                    $q->where('published_at', '<=', now()->toDateTimeString());
                }
            });
    }

    /**
     * Scope a query to only include unpublished posts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnpublished($query)
    {
        return $query->whereNull('published_at');
    }

    /**
     * Find a post by its primary slug or throw an exception.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $slug
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function scopeFindBySlugOrFail($query, $slug)
    {
        return $query->whereSlug($slug)->firstOrFail();
    }

    /**
     * Get the post's detail url.
     *
     * @return string
     */
    public function getDetailUrlAttribute()
    {
        return route('posts.show', $this->slug);
    }

    /*
     * Check post is published or not.
     */
    public function isPublished()
    {
        return $this->published_at;
    }

    /*
     * Publish the post.
     */
    public function publish($datetime = '')
    {
        $this->update([
            'published_at' => $datetime
                ? Carbon::createFromFormat('m/d/Y h:i A', $datetime) : now(),
        ]);
    }

    /**
     * Unpublish the post.
     */
    public function unpublish()
    {
        $this->update([
            'published_at' => null,
        ]);
    }

    /**
     * Scope a query to filter posts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, Request $request)
    {
        if ($request->filled('datetime')) {
            $dates = explode(' - ', $request->get('datetime'));

            $query->whereDate('created_at', '>=', Carbon::createFromFormat('m/d/Y', $dates[0])->toDateString());

            $query->whereDate('created_at', '<=', Carbon::createFromFormat('m/d/Y', $dates[1])->toDateString());
        }

        if ($request->filled('status')) {
            if ($request->get('status') == 'publish') {
                $query->published(true);
            } else {
                $query->unpublished();
            }
        }

        if ($request->filled('sort')) {
            if ($request->get('sort') == 'newest') {
                $query->orderBy('created_at', 'desc');
            } else {
                $query->orderBy('created_at', 'asc');
            }

        } else {
            $query->latest('created_at');
        }

        return $query;
    }

    /**
     * Scope a query to sort posts by published_at.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $column
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest($query, $column = 'published_at')
    {
        return $query->orderBy($column, 'desc');
    }
}
