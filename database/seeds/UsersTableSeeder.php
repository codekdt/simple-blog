<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->states('admin')->create([
            'name' => 'Admin',
            'email' => 'admin@test.com',
        ]);

        $user = factory(App\User::class)->create([
            'name' => 'Registered User',
            'email' => 'user@test.com',
        ]);

        $user->posts()->saveMany(factory(\App\Post::class, 5)->make());
        $user->posts()->saveMany(factory(\App\Post::class, 20)->states('published')->make());
    }
}
