<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReviewPostTest extends TestCase
{
    use CreatesPosts, RefreshDatabase;

    public function test_a_default_user_cannot_access_the_review_section()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->get('/review')
            ->assertRedirect('home');
    }

    public function test_an_admin_can_access_the_review_section()
    {
        $admin = factory(User::class)
            ->states('admin')
            ->create();

        $this->actingAs($admin)
            ->get('/review')
            ->assertStatus(200);
    }

    public function test_a_default_user_cannot_publish_post()
    {
        $post = $this->createPost();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->put('/posts/'.$post->id.'/publish')
            ->assertRedirect('home');
    }

    public function test_an_admin_can_publish_a_post()
    {
        $post = $this->createPost();

        $admin = factory(User::class)
            ->states('admin')
            ->create();

        $this->actingAs($admin)
            ->put('/posts/'.$post->id.'/publish')
            ->assertStatus(200);

        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
            'published_at' => null,
        ]);
    }

    public function test_an_admin_can_unpublish_a_post()
    {
        $post = $this->createPost(null, true);

        $admin = factory(User::class)
            ->states('admin')
            ->create();

        $this->actingAs($admin)
            ->put('/posts/'.$post->id.'/unpublish')
            ->assertStatus(200);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'published_at' => null,
        ]);
    }

    public function test_admin_can_filter_posts_by_status()
    {
        $unpublishedPost = $this->createPost();
        $publishedPost = $this->createPost(null, true);

        $admin = factory(User::class)
            ->states('admin')
            ->create();

        $this->actingAs($admin)
            ->get('/review?status=publish')
            ->assertStatus(200)
            ->assertViewHas('posts', function($posts) use ($publishedPost) {
                return $posts->contains($publishedPost);
            })
            ->assertViewHas('posts', function($posts) use ($unpublishedPost) {
                return ! $posts->contains($unpublishedPost);
            });
    }

    public function test_an_admin_can_schedule_publish_post()
    {
        $post = $this->createPost();

        $admin = factory(User::class)
            ->states('admin')
            ->create();

        $this->actingAs($admin)
            ->put('/posts/'.$post->id.'/publish-at', [
                'datetime' => '01/01/2019 09:00 AM'
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'published_at' => '2019-01-01 09:00:00',
        ]);
    }
}
