<?php

namespace Tests\Unit;

use App\User;
use App\Post;

trait CreatesPosts
{
    /**
     * Create a new team instance.
     *
     * @param User $user
     * @param $publish
     * @return Post
     */
    public function createPost($user = null, $publish = false)
    {
        $user = $user ?: factory(User::class)->create();

        $post = (new Post)->forceFill([
            'title' => 'Title',
            'user_id' => $user->id,
            'content' => 'Content',
            'published_at' => $publish ? now() : null,
        ]);

        $user->posts()->save($post);

        return $post->fresh();
    }
}