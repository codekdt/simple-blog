<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeletePostTest extends TestCase
{
    use CreatesPosts, RefreshDatabase;

    public function test_post_can_be_not_delete_by_other_user()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $post = $this->createPost($user1);

        $this->actingAs($user2)
            ->delete("/posts/{$post->id}")
            ->assertNotFound();
    }

    public function test_post_can_be_deleted()
    {
        $post = $this->createPost();

        $this->actingAs($post->user)
            ->delete("/posts/{$post->id}")
            ->assertStatus(200);

        $this->assertSoftDeleted('posts', [
            'id' => $post->id
        ]);
    }
}
