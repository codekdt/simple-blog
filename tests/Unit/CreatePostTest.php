<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatePostTest extends TestCase
{
    use RefreshDatabase;

    public function test_non_login_user_can_not_create_post()
    {
        $this->get('/posts/create')->assertStatus(302);
    }

    public function test_an_login_user_can_create_post()
    {
        $this->actingAs(factory(User::class)->create())
            ->get('/posts/create')
            ->assertStatus(200);
    }

    public function test_post_can_be_created()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->post('posts', [
                'title' => 'The title',
                'content' => '# Hello world'
            ])
            ->assertLocation(route('posts.manager'));

        $this->assertDatabaseHas('posts', [
            'title' => 'The title',
            'user_id' => $user->id,
        ]);
    }

    public function test_title_is_required()
    {
        $this->actingAs(factory(User::class)->create())
            ->post('posts', [
                'content' => '# Bla bla',
            ])
            ->assertSessionHasErrors();
    }
}
