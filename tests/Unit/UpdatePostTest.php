<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdatePostTest extends TestCase
{
    use CreatesPosts, RefreshDatabase;

    public function test_a_post_can_be_not_edit_by_other_user()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $post = $this->createPost($user1);

        $this->actingAs($user2)
            ->get("/posts/{$post->id}/edit")
            ->assertStatus(404);
    }

    public function test_post_can_be_updated()
    {
        $post = $this->createPost();

        $this->actingAs($post->user)
            ->put("/posts/{$post->id}", [
                'title' => 'Title (Updated)',
                'content' => 'Content'
            ])
            ->assertLocation(route('posts.edit', $post->id));

        $this->assertDatabaseHas('posts', [
            'title' => 'Title (Updated)',
        ]);
    }

    public function test_title_is_required()
    {
        $post = $this->createPost();

        $this->actingAs($post->user)
            ->put("/posts/{$post->id}", [
                'content' => 'Content'
            ])
            ->assertSessionHasErrors();
    }
}
