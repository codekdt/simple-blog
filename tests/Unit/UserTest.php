<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_default_user_is_not_an_admin()
    {
        $user = factory(User::class)->create();

        $this->assertFalse($user->isAdmin());
    }

    public function test_a_admin_user_is_an_admin()
    {
        $user = factory(User::class)->states('admin')->create();

        $this->assertTrue($user->isAdmin());
    }

    public function test_an_user_has_posts()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $user->posts
        );
    }
}
